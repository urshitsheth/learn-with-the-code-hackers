def days(no):
    switcher = {
        1: "Monday",
        2: "Tuesday",
        3: "Wednesday",
        4: "Thursday",
        5: "Friday",
        6: "Saturday",
        7: "Sunday"
    }
    return switcher.get(no, "Wrong day")


number = int(input("Enter The Number :- "))
print("Today's Day is " + days(number))
